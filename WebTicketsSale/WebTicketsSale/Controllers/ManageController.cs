﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using WebTicketsSale.Models.MyClasses;
using WebTicketsSale.Models.MyClasses.Repository;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebTicketsSale.Models.ManageViewModels;
using Microsoft.Extensions.Localization;
using System.Linq;
using System.Collections.Generic;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebTicketsSale.Controllers
{

    [Authorize]
    public class ManageController : Controller
    {
        private readonly IStringLocalizer<ManageController> _localizer;
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;       
        private readonly ILogger logger;
        private readonly string externalCookieScheme;
        private IRepository _repos;

        public ManageController(
          UserManager<User> userManager,
          SignInManager<User> signInManager,
          IOptions<IdentityCookieOptions> identityCookieOptions,                
          ILoggerFactory loggerFactory,
          IStringLocalizer<ManageController> localizer,
          IRepository repos)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            externalCookieScheme = identityCookieOptions.Value.ExternalCookieAuthenticationScheme;          
            logger = loggerFactory.CreateLogger<ManageController>();
            _localizer = localizer;
            _repos = repos;
        }

        [HttpGet]
        public IActionResult MyTickets()
        {            
            return View();
        }
        
        [HttpGet]
        public IActionResult Selling(string Id)
        {
            var List = _repos.GetAllTicket.Where(xx => xx.UserId.Equals(Id)).ToList();
            
            return Json(List);
        }

        [HttpGet]
        public IActionResult Waiting(string Id)
        {            
            var listOrders = _repos.GetAllOrder.Where(xx => xx.Ticket.UserId.Equals(Id) && xx.Status==Status.waiting_for_confirmation).ToList();            

            return Json(listOrders);
        }
        [HttpGet]
        public IActionResult Sold(string Id)
        {
            var listOrders = _repos.GetAllOrder.Where(xx => xx.Ticket.UserId.Equals(Id) && xx.Status == Status.confirmed).ToList();

            return Json(listOrders);
        }

        public IActionResult MyOrders(string Id)
        {
            var listOrders = _repos.GetAllOrder.Where(xx => xx.Buyer.UserId == Id).ToList();
            return View(listOrders);
        }

        [HttpGet]
        public IActionResult ListUsers()
        {
            IQueryable<User> list = userManager.Users; 
            return View(list);
        }        

        [HttpGet]
        public async Task<IActionResult> Index(ManageMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."                
                : message == ManageMessageId.Error ? "An error has occurred."                
                : "";

            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var model = new IndexViewModel
            {
                HasPassword = await userManager.HasPasswordAsync(user),
                
            };
            return View(model);
        }

        // GET: /Manage/ChangePassword
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent: false);
                    logger.LogInformation(3, "User changed their password successfully.");
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }

        //
        // GET: /Manage/SetPassword
        [HttpGet]
        public IActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await userManager.AddPasswordAsync(user, model.NewPassword);
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }
        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        public enum ManageMessageId
        {    
            ChangePasswordSuccess,            
            SetPasswordSuccess,
            Error
        }
        private Task<User> GetCurrentUserAsync()
        {
            return userManager.GetUserAsync(HttpContext.User);
        }
#endregion

    }
}
