using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebTicketsSale.Models.MyClasses;
using WebTicketsSale.Models.MyClasses.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;

namespace WebTicketsSale.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStringLocalizer<HomeController> _localizer;
        IRepository _repos;
        public HomeController(IStringLocalizer<HomeController> localizer, IRepository repos)
        {
            _localizer = localizer;
            _repos = repos;
        }
        public IActionResult Index()
        {
            List<string> val = new List<string>();
            foreach(EventType e in Enum.GetValues(typeof(EventType)))
            {
                val.Add(e.ToString());
            }          
                     
            return View(val);
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }



        public IActionResult Concert(string Id)
        {
            var ev = _repos.GetEvents(Id);
                       
            ViewBag.Title = _localizer[Id];
            return View(ev);
        }
        [HttpGet]
        public IActionResult AboutEvent(int Id)
        {
            MyEvent me = _repos.GetEvent(Id);
            return Json(me.Description);
        }


        [HttpGet]
        [Authorize(Roles = "Admin, User")]        
        public IActionResult Tickets(int Id)
        {
            List<Ticket> tickets = _repos.GetAllTicket.Where(xx => xx.MyEventId == Id).ToList();            
            return View(tickets);
        }
    }
}