﻿$(document).ready(function () {
    $(document).on('mouseover', '.wrap_events', function () {
        $(this).css("border-color", "red");
        $(this).css("color", "red");
    });
    $(document).on('mouseout', '.wrap_events', function () {
        $(this).css("border-color", "yellow");
        $(this).css("color", "white");
    });
    $("div.events_name")
        .mouseover(function () {
            $(this).css("border-color", "red");
            $(this).css("color", "red");
        }).mouseout(function () {
            $(this).css("border-color", "yellow");
            $(this).css("color", "white");
        });   
});