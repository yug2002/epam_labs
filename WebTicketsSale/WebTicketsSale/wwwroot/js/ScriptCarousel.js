// JavaScript source code
$(document).ready(function () {
    var list = document.getElementsByClassName('item');

    (function ($) {
        $.fn.Carousel = function (options) {
            //��������� �� ���������
            var settings = {
                visible: 1,  //���������� ������������ ������� 3
                rotateBy: 1, //������������ �� 1
                speed: 1000, //�������� 1 �������
                btnNext: null, // ������ ������ �� ���������
                btnPrev: null, // ������ ����� �� ���������
                auto: true, // ���� ��������� ��������
                margin: 10,    // ������ ����� ���������
                position: "h",// ������������ �� �����������
                dirAutoSlide: false //����������� �������� � ����� ��� �������������
            };
            return this.each(function () {
                if (options) {
                    $.extend(settings, options); //������������� ���������������� ��������� 
                }
                // ���������� ����������
                var $this = $(this);//������������ ������� (���� � ������� ��������� ��������)                      
                var $carousel = $this.children(':first');// �������� �������� ������� (UL?) �.�. ���� ��������

                var itemWidth = $carousel.children().outerWidth() + settings.margin; // ��������� ������ ��������
                var itemHeight = $carousel.children().outerHeight() + settings.margin;// ��������� ������ ��������                   
                var itemsTotal = $carousel.children().length; // �������� ����� ���������� ��������� � ��������

                var running = false; //������������� �������
                var intID = null; //�������� ��������

                //size - ������ ��� ���������� �����, ������� �� ���������� ��������
                var size = itemWidth; 
                $this.css({
                    'position': 'relative', // ���������� ��� ����������� ����������� � ��6(7)
                    'overflow': 'hidden', // ������ ���, ��� �� ������� � ���������
                    'width': settings.visible * size + 'px',// ������ ���������� ������ ������ ������ ���� ������� ���������
                    'height': itemHeight - settings.margin
                });
                //��������� ���������� ������� �� ������� ��������
                
                $carousel.children('item').css({
                    'margin-left': settings.margin / 2 + 'px',
                    'margin-right': settings.margin / 2 + 'px',
                });
                // ����������� ������ ��������
                
                $carousel.css({
                    'position': 'relative', // ��������� ����� �� ���
                    'width': 9999 + 'px', // ����������� ���� ��������
                    'top': 0,
                    'left': 0
                });
                //��������� �������� � ���������� dir [true-������; false-�����]
                function slide(dir) {
                    var direction = !dir ? -1 : 1; // ������������� �������� �����������
                    var Indent = 0; // �������� (��� ul)
                    if (!running) {
                        // ���� �������� ��������� (��� ��� �� ��������)
                        running = true; // ������ ������, ��� �������� � ��������
                        if (intID) { // ���� ������� ��������
                            window.clearInterval(intID); // ������� ��������                                         
                        }
                        if (!dir) { // ���� �� ������ � ���������� �������� (��� �� ���������)
                            /*
                              * ��������� ����� ���������� �������� ��������
                              * ����� �������� ���������, ������� ������
                              * � ��������� rotateBy (�� ��������� ����� ���� �������)
                            */
                            $carousel.children(':last').after($carousel.children().slice(0, settings.rotateBy).clone(true));
                        } else { // ���� ������ � ����������� ��������
           /*
              * ��������� ����� ������ ��������� ��������
              * ����� �������� ���������, ������� ������
              * � ��������� rotateBy (�� ��������� ����� ���� �������)
           */                                                $carousel.children(':first').before($carousel.children().slice(itemsTotal - settings.rotateBy, itemsTotal).clone(true));
                            /*
                             * �������� �������� (<ul>)  �� ������/������  ��������,
                             * ���������� �� ���������� ���������, ��������
                             * � ��������� rotateBy (�� ��������� ����� ���� �������)
                            */
                            if (settings.position == "v")
                                $carousel.css('top', -size * settings.rotateBy + 'px');
                            else $carousel.css('left', -size * settings.rotateBy + 'px');
                        }

                        /*
                         * �����������  ��������
                         * ������� ��������  + ������/������  ������ �������� * ���������� ������������� ��������� * �� ����������� ����������� (1 ��� -1)
                          */
                        if (settings.position == "v")
                            Indent = parseInt($carousel.css('top')) + (size * settings.rotateBy * direction);
                        else
                            Indent = parseInt($carousel.css('left')) + (size * settings.rotateBy * direction);
                        
                            var animate_data = { 'left': Indent };
                        // ��������� ��������
                        $carousel.animate(animate_data, {
                            queue: false, duration: settings.speed, complete: function () {
                                // ����� �������� ���������
                                if (!dir) { // ���� �� ������ � ���������� �������� (��� �� ���������)
                                    // ������� ������� ������ ���������, ������� ������ � rotateBy
                                    $carousel.children().slice(0, settings.rotateBy).remove();
                                    // ������������� ����� � ����                                    
                                     $carousel.css('left', 0);
                                } else { // ���� ������ � ����������� ��������
                                    // ������� ������� ��������� ���������, ������� ������ � rotateBy
                                    $carousel.children().slice(itemsTotal, itemsTotal + settings.rotateBy).remove();
                                }
                                if (settings.auto) { // ���� �������� ������ ������������� �������������
                                    // ��������� ����� ������� ����� �������� ������� (auto)
                                    intID = window.setInterval(function () { slide(settings.dirAutoSlide); }, settings.auto);
                                }
                                running = false; // ��������, ��� �������� ���������
                            }
                        });
                    }
                    return false; // ���������� false ��� ����, ����� �� ���� �������� �� ������
                }
                // ��������� ���������� �� ������� click ��� ������ "������"
                $(settings.btnNext).click(function () {
                    return slide(false);
                });
                // ��������� ���������� �� ������� click ��� ������ "�����"
                $(settings.btnPrev).click(function () {
                    return slide(true);
                });

                if (settings.auto) { // ���� �������� ������ ������������� �������������
                    // ��������� ����� ������� ����� ��������� ��������
                    intID = window.setInterval(function () { slide(settings.dirAutoSlide); }, settings.auto);
                }
            });
        };

    })(jQuery);
    $('.carousel').Carousel({
        visible: 1,
        rotateBy: 1,
        speed: 1000,
        btnNext: '.carousel-button-right',
        btnPrev: '.carousel-button-left',
        auto: false,
        backslide: true,
        margin: 10
    });


});
