﻿$(document).ready(function () {
    $(".button-info").click(function () {
        
        var curDiv = $(this);
        var curr = curDiv.data("content");        

        $.get("/Home/AboutEvent", { Id: curr },
            function (data) {
                $("#popup").text("");
                $("#popup").append("<p>" + data + "</p>");
                $("#popup").show("normal");
                $("#hover").show("fast");
            });        
                
    });
    $("#hover").click(function () {
        $("#popup").hide("normal");
        $("#hover").hide("fast");
        $("#form").hide("normal");
        return false;
    });
    
});