﻿$(document).ready(function () {   
   
    $("a.selling").click(function () {
        $("a.selling").css('background-color', '#C73B37');
        $("a.sold").css('background-color', '#2246B7');
        $("a.waiting").css('background-color', '#2246B7');
        $("div.my-tickets").empty();
        var curDiv = $(this);
        var curr = curDiv.data("content");
        var divv = $(".my-tickets");
        
        $.get("/Manage/Selling", { Id: curr },
            function (tickets) {                
                $.each(tickets, function (i) {                                     
                   
                    divv.append("<div></div>");
                    $('.my-tickets>div').addClass('wrap_events').each(function (i) {
                        if ($(this).children().length === 0) {                           
                            $(this).append("<div>" + tickets[i].price + "</div>");                            
                            $(this).append("<div>" + tickets[i].myEvent.name + "</div>");
                            $(this).append("<div>" + tickets[i].myEvent.venue.city.name + "</div>");
                            $(this).append("<div>" + tickets[i].myEvent.venue.name + "</div>");
                            $(this).append("<div>" + $.date(tickets[i].myEvent.date) + "</div>");
                            $('.wrap_events > div').addClass('events');
                        }                        
                        return true;
                    });
                   
                });                
            });
    });

    $("a.waiting").click(function () {
        $("a.selling").css('background-color', '#2246B7');
        $("a.waiting").css('background-color', '#C73B37');
        $("a.sold").css('background-color', '#2246B7');
        $("div.my-tickets").empty();
        var curDiv = $(this);
        var curr = curDiv.data("content");
        var divv = $(".my-tickets");

        $.get("/Manage/Waiting", { Id: curr },
            function (orders) {
                $.each(orders, function (i) {

                    divv.append("<div></div>");
                    $('.my-tickets>div').addClass('wrap_events').each(function (i) {
                        if ($(this).children().length === 0) {
                            $(this).append("<div>" + orders[i].ticket.price + "</div>");
                            $(this).append("<div>" + orders[i].ticket.myEvent.name + "</div>");
                            $(this).append("<div>" + orders[i].ticket.myEvent.venue.city.name + "</div>");
                            $(this).append("<div>" + $.date(orders[i].ticket.myEvent.date) + "</div>");
                            $(this).append("<div>" + orders[i].buyer.firstName + "</div>");
                            $(this).append("<div>" + orders[i].buyer.lastName + "</div>");
                            //$(this).append("<div>" + orders[i].buyer.phoneNumber + "</div>");
                            $('.wrap_events > div').addClass('events');
                        }
                        return true;
                    });

                });
            });
    });

    $("a.sold").click(function () {
        $("a.sold").css('background-color', '#C73B37');
        $("a.selling").css('background-color', '#2246B7');
        $("a.waiting").css('background-color', '#2246B7');
        $("div.my-tickets").empty();
        var curDiv = $(this);
        var curr = curDiv.data("content");
        var divv = $(".my-tickets");

        $.get("/Manage/Sold", { Id: curr },
            function (orders) {
                $.each(orders, function (i) {

                    divv.append("<div></div>");
                    $('.my-tickets>div').addClass('wrap_events').each(function (i) {
                        if ($(this).children().length === 0) {
                            $(this).append("<div>" + orders[i].ticket.price + "</div>");
                            $(this).append("<div>" + orders[i].ticket.myEvent.name + "</div>");
                            $(this).append("<div>" + orders[i].ticket.myEvent.venue.city.name + "</div>");
                            $(this).append("<div>" + $.date(orders[i].ticket.myEvent.date) + "</div>");
                            $(this).append("<div>" + orders[i].buyer.firstName + "</div>");
                            $(this).append("<div>" + orders[i].buyer.lastName + "</div>");
                            //$(this).append("<div>" + orders[i].buyer.phoneNumber + "</div>");
                            $('.wrap_events > div').addClass('events');
                        }
                        return true;
                    });

                });
            });
    });
    $.date = function (dateObject) {
        var d = new Date(dateObject);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = day + "." + month + "." + year;
        return date;
    };
});
    
