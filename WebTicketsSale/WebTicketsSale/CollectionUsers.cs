﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using WebTicketsSale.Models.MyClasses;
using WebTicketsSale.Models.MyClasses.Repository;

namespace WebTicketsSale
{
    public class CollectionUsers : IUserStore<User>, IUserEmailStore<User>, IUserPasswordStore<User>, IUserRoleStore<User>, IQueryableUserStore<User>

    {
        private static readonly List<User> collectionUser;
        

        static CollectionUsers()
        {
            collectionUser = TicketsOrderRepository.AllUsers.ToList(); 
        }
        public CollectionUsers()
        {
            
        }
        
        public static int Count
        {
            get
            {
                return collectionUser.Count;
            }                      
        }

        public IQueryable<User> Users => collectionUser.AsQueryable();

        public Task AddToRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            user.UserRole.RoleName = roleName;
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            user.UserId = Guid.NewGuid().ToString();
            collectionUser.Add(user);
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            var match = collectionUser.FirstOrDefault(u => u.UserId == user.UserId);
            if (match != null)
            {
                collectionUser.Remove(match);

                return Task.FromResult(IdentityResult.Success);
            }
            
            return Task.FromResult(IdentityResult.Failed());
            
        }

        public void Dispose() {  }

        public Task<User> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            var user = collectionUser.FirstOrDefault(u => String.Equals(u.NormEmail, normalizedEmail, StringComparison.OrdinalIgnoreCase));

            return Task.FromResult(user);
        }

        public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            var user = collectionUser.FirstOrDefault(u => u.UserId == userId);

            return Task.FromResult(user);
        }
        
        public Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            var user = collectionUser.FirstOrDefault(u => String.Equals(u.NormLastName, normalizedUserName, StringComparison.OrdinalIgnoreCase));

            return Task.FromResult(user);
        }

        public Task<string> GetEmailAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }        

        public Task<string> GetNormalizedEmailAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormEmail);
        }

        public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormLastName);
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<IList<string>> GetRolesAsync(User user, CancellationToken cancellationToken)
        {
            IList<string> roles = new List<string>();
            roles.Add(user.UserRole.RoleName);
            return Task.FromResult(roles);
        }

        public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserId);
        }

        public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.LastName);
        }

        public Task<IList<User>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            List<User> temp = new List<User>(); 
            temp = collectionUser.Where(xx => xx.UserRole.RoleNameNormalized.Equals(roleName)).ToList();
            IList<User> listUsersInRole = collectionUser.Where(xx => xx.UserRole.RoleNameNormalized.Equals(roleName)).ToList();
            return Task.FromResult(listUsersInRole);
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public Task<bool> IsInRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            if(user.UserRole.RoleName!=null)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false); ;
        }

        public Task RemoveFromRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task SetEmailAsync(User user, string email, CancellationToken cancellationToken)
        {
            user.Email = email;

            return Task.CompletedTask;
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task SetNormalizedEmailAsync(User user, string normalizedEmail, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;

            return Task.FromResult(true);
        }

        public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            user.LastName = userName;
            return Task.FromResult(true);
        }

        public Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            var match = collectionUser.FirstOrDefault(u => u.UserId == user.UserId);
            if (match != null)
            {
                match.LastName = user.LastName;
                match.Email = user.Email;
                match.PhoneNumber = user.PhoneNumber;                
                match.PasswordHash = user.PasswordHash;

                return Task.FromResult(IdentityResult.Success);
            }
            
            return Task.FromResult(IdentityResult.Failed());
           
        }

        
    }
}
