﻿using System.ComponentModel.DataAnnotations;

namespace WebTicketsSale.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name ="LastName")]
        public string LastName { get; set; }

        //[Required]
        //[EmailAddress]
        //[Display(Name = "Email")]
        //public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "MinMaxPassword", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm_password")]
        [Compare("Password", ErrorMessage = "Thepasswordandconfirmationpassworddonotmatch")]
        public string ConfirmPassword { get; set; }
    }
}
