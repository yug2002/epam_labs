﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTicketsSale.Models.MyClasses
{
    public enum Status { waiting_for_confirmation, confirmed, rejected }
    public class Order
    {
        public int Id { get; set; }
        public Status Status { get; set; }

        public string UserId { get; set; }
        public User Buyer { get; set; }

        public string TrackNO { get; set; }

        public Ticket Ticket { get; set; }

        public Order()
        {
            Id = 0;
            Status = Status.rejected;
            UserId = "";
            Buyer = null;
            TrackNO = "";
            Ticket = new Ticket();
        }

        public Order(int id, Status status, string userId, User buyer, string track, Ticket ticket)
        {
            Id = id;
            Status = status;
            UserId = userId;
            Buyer = buyer;
            TrackNO = track;
            Ticket = ticket;                      
        }
    }
}
