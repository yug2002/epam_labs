﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTicketsSale.Models.MyClasses
{
    public class Venue
    {
        public int VenueId { get; set; }
        public string Name { get; set; }
        public string Addres { get; set; }

        public int CityId { get; set; }
        public City City { get; set; }

        public Venue()
        {
            VenueId = 0;
            Name = default(string);
            Addres = default(string);
            CityId = 0;
            City = null;
        }

        public Venue(int id, string name, string address, int cityId, City city)
        {
            VenueId = id;
            Name = name;
            Addres = address;
            CityId = cityId;
            City = city;
        }
    }
}
