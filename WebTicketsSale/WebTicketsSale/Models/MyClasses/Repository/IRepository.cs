﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTicketsSale.Models.MyClasses.Repository
{
    public interface IRepository
    {
        IEnumerable<Ticket> GetAllTicket { get; }
        IEnumerable<Order> GetAllOrder { get; } 
        IEnumerable<User> GetAllUsers { get; }
        IEnumerable<MyEvent> GetAllEvents { get; }
        IEnumerable<MyEvent> GetEvents(string str);
        MyEvent GetEvent(int id);
    }
}
