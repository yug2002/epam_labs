﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTicketsSale.Models.MyClasses.Repository
{
    public class TicketsOrderRepository : IRepository
    {
        private static List<Ticket> collectionTickets;
        private static List<Order> collectionOrders;
        private static List<User> collectionUsers;
        private static List<MyEvent> collectionEvents;

        static TicketsOrderRepository()
        {
            City gomel = new City(1, "Gomel");
            City minsk = new City(2, "Minsk");
            City brest = new City(3, "Brest");
            City mogilev = new City(4, "Mogilev");
            City grodno = new City(5, "Grodno");
            City vitebsk = new City(6, "Vitebsk");

            Venue gomelOne = new Venue(1, "Socio-Сultural Сenter", "Lange str. 17", 1, gomel);
            Venue gomelTwo = new Venue(2, "Ice Arena", "Mazurova str. 110", 1, gomel);
            Venue gomelThree = new Venue(3, "Сentral Stadium", "Vosstaniya sq. 1", 1, gomel);

            Venue minskOne = new Venue(4, "The Victory Cinema", "Internazionalnaya str. 20", 2, minsk);
            Venue minskTwo = new Venue(5, "The Dynamo Stadium", "Kirova str. 8", 2, minsk);
            Venue minskThree = new Venue(6, "Aquapark", "Pobeditelej av. 120", 2, minsk);

            Venue brestOne = new Venue(7, "Philharmonic society", "Ordzhonikidze str. 14", 3, brest);
            Venue brestTwo = new Venue(8, "Concert hall", "Skripnikova str. 45", 3, brest);
            Venue brestThree = new Venue(9, "Brest Social and Cultural Center", "Kommunisticheskaya str. 1", 3, brest);

            Venue mogilevOne = new Venue(10, "Museum of Mogilev History", "Boldina str., 1", 4, mogilev);
            Venue mogilevTwo = new Venue(11, "Local History Museum", "Sovetskaya str., 1", 4, mogilev);

            Venue grodnoOne = new Venue(12, "Ice Palace of Sports", "Kommunalnaya str, 3a", 5, grodno);
            Venue grodnoTwo = new Venue(13, "Ice Palace of Sports", "Kommunalnaya str, 3a", 5, grodno);

            Venue vitebskOne = new Venue(14, "Summer amphitheater", "Frunze av., 13a", 6, vitebsk);
            Venue vitebskTwo = new Venue(15, "Local History Museum", "Lenina str., 36", 6, vitebsk);

            MyEvent myEventOne = new MyEvent(1, "A. Pugacheva", new DateTime(2017, 06, 01), "\\images\\Banners\\Concert\\pug50.jpg", "А́лла Бори́совна Пугачёва — советская и российская эстрадная певица, композитор-песенник, эстрадный режиссёр, продюсер, киноактриса и телеведущая. Народная артистка СССР. Лауреат Государственной премии Российской Федерации.", 1, gomelOne, EventType.Concert);
            MyEvent myEventTwo = new MyEvent(2, "Hockey match in Gomel", new DateTime(2017, 06, 09), "\\images\\Banners\\Sport\\hoc50.jpg", "Хоккейный матч Бейбарыс -  Алматы", 2, gomelTwo, EventType.Sport);
            MyEvent myEventThree = new MyEvent(3, "Football match in Gomel", new DateTime(2017, 07, 01), "\\images\\Banners\\Sport\\fut50.jpg", "Футбольный матч Гомсельмаш - Ювентус", 3, gomelThree, EventType.Sport);

            MyEvent myEventFour = new MyEvent(4, "Film Festival 'Listapad'", new DateTime(2017, 06, 14), "\\images\\Banners\\Other\\list50.jpg", "Каждый год в ноябре ММКФ «Лістапад» привозит в Минск лучшие картины со всего мира. Основные конкурсы игрового и документального кино сфокусированы на новых фильмах, созданных в странах бывшего социалистического лагеря: странах СНГ, Центральной и Юго-Восточной Азии, странах Балтии, Восточной и Центральной Европы. На главную награду Минского МКФ претендуют картины этого региона, получившие признание самых престижных кинофестивалей мира. Конкурс «Молодость на марше» представляет коллекцию дебютных работ авторов со всего мира, уже завоевавших признание профессионалов и зрителей.", 4, minskOne, EventType.Other);
            MyEvent myEventFive = new MyEvent(5, "Pink Floyd", new DateTime(2017, 06, 22), "\\images\\Banners\\Concert\\pink_floyd_50.jpg", "Pink Floyd — британская рок-группа, знаменитая своими философскими текстами, акустическими экспериментами, инновациями в оформлении альбомов и грандиозными шоу.", 5, minskTwo, EventType.Concert);
            MyEvent myEventSix = new MyEvent(6, "Aqua Baby Dance", new DateTime(2017, 07, 11), "\\images\\Banners\\Show\\aq50.jpg", "Как правило, дети относятся к отдыху иначе, чем их родители. Дети любят шуметь и веселиться, маме и папе хочется отдохнуть и расслабиться. Но есть главное желание, самое важное для ребят – чтобы всё было «по-взрослому». Вы хотите, чтобы отдых для ребёнка прошёл с максимальной отдачей? Детская дискотека в акватории – это замечательная возможность исполнить их желание! Развлекать деток будет аниматор, который будет также учить их различным танцевальным движениям! ", 6, minskThree, EventType.Show);

            MyEvent myEventSeven = new MyEvent(7, "Rich sound band", new DateTime(2017, 06, 29), "\\images\\Banners\\Concert\\rich50.jpg", "Вас ждет вечер живой музыки, море позитива, океан воспоминаний, романтическое настроение и безудержное танцевальное веселье. Приглашаем Вас приятно провести время в приятной компании за чашечкой чая. ", 7, brestOne, EventType.Concert);
            MyEvent myEventEight = new MyEvent(8, "Alexander Malinin", new DateTime(2017, 09, 09), "\\images\\Banners\\Concert\\malinin50.jpg", "Алекса́ндр Никола́евич Мали́нин — советский и российский эстрадный певец, композитор, актёр. Заслуженный артист РСФСР, народный артист России, народный артист Украины.", 8, brestTwo, EventType.Concert);
            MyEvent myEventNine = new MyEvent(9, "Oleg Vinnik", new DateTime(2017, 08, 14), "\\images\\Banners\\Concert\\vinnik50.jpg", "Оле́г Анато́льевич Ви́нник — украинский певец, композитор и автор песен. Получил известность в Германии, Австрии и Швейцарии как исполнитель главных ролей популярных мюзиклов под сценическим именем OLEGG.", 9, brestThree, EventType.Concert);

            MyEvent myEventTen = new MyEvent(10, "Football match in Minsk", new DateTime(2017, 08, 02), "\\images\\Banners\\Sport\\fut_m50.jpg", "Футбольный матч  'Динамо Минск' - 'Шахтер Караганда'", 5, minskTwo, EventType.Sport);

            MyEvent myEventElev = new MyEvent(11, "Historical images", new DateTime(2017, 09, 11), "\\images\\Banners\\Exhibition\\mog_hist50.jpg", "Коллекция «Изобразительного искусства» была сформирована в результате активной собирательской деятельности сотрудников музея в период с 1949 г. и по сегодняшний день. Коллекция имеет три раздела: живопись, графика, скульптура. Дает возможность проследить основные этапы развития белорусского и могилевского изобразительного искусства за послевоенный период.", 10, mogilevOne, EventType.Exhibition);
            MyEvent myEventTwel = new MyEvent(12, "Space of Yazep Drazdovich", new DateTime(2017, 10, 03), "\\images\\Banners\\Exhibition\\yaz50.jpg", "Язеп Дроздович (1888–1954) – наиболее значимая фигура первой волны белорусского культурного возрождения XX в., писатель, поэт, этнограф, скульптор, философ, хиромант, археолог, театральный деятель и астроном-любитель. Но известен прежде всего как художник.",11, mogilevTwo, EventType.Exhibition);

            MyEvent myEventThir = new MyEvent(13, "Hockey match", new DateTime(2017, 11, 05), "path/Banner", "Хоккейный матч между командами Австралии: Аделаида - Мельбурн Айс", 12, grodnoOne, EventType.Sport);
            MyEvent myEventFourt = new MyEvent(14, "Food Show", new DateTime(2017, 11, 11), "path/Banner", "Здесь описание", 13, grodnoTwo, EventType.Show);

            MyEvent myEventFift = new MyEvent(15, "Lyapis-98", new DateTime(2017, 12, 02), "path/Banner", "Здесь описание", 14, vitebskOne, EventType.Concert);
            MyEvent myEventSixt = new MyEvent(16, "Art project 'Signs and ciphers'", new DateTime(2017, 12, 10), "path/Banner", "Здесь описание", 15, vitebskTwo, EventType.Exhibition);

            collectionEvents = new List<MyEvent>()
            {
                myEventOne, myEventTwo, myEventThree, myEventFour,
                myEventFive, myEventSix, myEventSeven, myEventEight,
                myEventNine, myEventTen, myEventElev, myEventTwel,
                myEventThir, myEventFourt, myEventFift, myEventSixt

            };

            PasswordHasher<User> ph = new PasswordHasher<User>();
            UserRole r1 = new UserRole();
            UserRole r2 = new UserRole();
            r1.RoleName = "User";
            r2.RoleName = "Admin";
            User userOne = new User("1", "Ivan", "User", "ivan@mail.ru", Localisation.russian, "Sovetskaya str, 105-13, Moscow", "+7445661254", "", r1);
            User userTwo = new User("2", "Petya", "Petrov", "pet@mail.ru", Localisation.belarussian, "Sviridova str, 14-55, Gomel", "+375295556655", "", r1);
            User userThree = new User("3", "John", "Smith", "john@mail.ru", Localisation.english, "First str, 22-114, London", "+122155668999", "", r1);
            User userFour = new User("4", "Vasya", "Vasin", "vasya@mail.ru", Localisation.russian, "Lenina av, 112-14, Moscow", "+7449998999", "", r1);
            User admin = new User("5", "Kolya", "Admin", "admin@mail.ru", Localisation.belarussian, "hide", "+37529111111", "", r2);

            userOne.PasswordHash = ph.HashPassword(userOne, "User");
            userTwo.PasswordHash = ph.HashPassword(userTwo, "qqqq");
            userThree.PasswordHash = ph.HashPassword(userThree, "qqqq");
            userFour.PasswordHash = ph.HashPassword(userFour, "qqqq");
            admin.PasswordHash = ph.HashPassword(admin, "Admin");

            collectionUsers = new List<User>()
            {
                userOne,userTwo,userThree,userFour,admin
            };

            collectionTickets = new List<Ticket>()
            {
                new Ticket(1, 1, myEventOne, 30.22m, "5", admin),
                new Ticket(2, 1, myEventOne, 45.00m, "5", admin),
                new Ticket(3, 2, myEventTwo, 65.00m, "2", userTwo),
                new Ticket(4, 2, myEventTwo, 70.00m, "2", userTwo),
                new Ticket(5, 2, myEventTwo, 70.00m, "2", userTwo),
                new Ticket(6, 3, myEventThree, 15.50m, "5", admin),
                new Ticket(7, 3, myEventThree, 14.00m, "5", admin),
                new Ticket(8, 3, myEventThree, 19.00m, "2", userTwo),
                new Ticket(9, 3, myEventThree, 14.00m, "4", userFour),
                new Ticket(10, 4, myEventFour, 12.00m, "3", userThree),
                new Ticket(11, 4, myEventFour, 12.00m, "3", userThree),
                new Ticket(12, 4, myEventFour, 12.00m, "3", userThree),
                new Ticket(13, 5, myEventFive, 25.05m, "1", userOne),
                new Ticket(14, 5, myEventFive, 25.05m, "2", userTwo),
                new Ticket(15, 6, myEventSix, 30.00m, "5", admin),
                new Ticket(16, 6, myEventSix, 35.00m, "1", userOne),
                new Ticket(17, 7, myEventSeven, 40.22m, "2", userTwo),
                new Ticket(18, 7, myEventSeven, 40.22m, "1", userOne),
                new Ticket(19, 8, myEventEight, 130.00m, "2", userTwo),
                new Ticket(20, 9, myEventNine, 130.00m, "1", userOne),
                new Ticket(21, 10, myEventTen, 50.50m, "4", userFour),
                new Ticket(22, 10, myEventTen, 50.50m, "3", userThree),
                new Ticket(23, 11, myEventElev, 2.0m, "2", userTwo),
                new Ticket(24, 12, myEventTwel, 20.0m, "5", admin)
            };

            var Bayer1 = new User("6", "Igor", "Igorov", "", Localisation.russian, "Sviridova str, 15-77, Gomel", "+375295656566", "", null);
            var Bayer2 = new User("7", "Lena", "Elenova", "", Localisation.russian, "Sovetskaya str, 115-74, Gomel", "+375295999566", "", null);
            var Bayer3 = new User("8", "Petya", "Potkin", "", Localisation.russian, "Lenina av, 155-77, Gomel", "+375295656566", "", null);
            var Bayer4 = new User("9", "Sasha", "Sashin", "", Localisation.russian, "Dyndy str, 1-56, Gomel", "+375295656566", "", null);
            User Bayer5 = new User("10", "Yury", "Krotov", "", Localisation.russian, "Sviridova str, 12-77, Gomel", "+375295656566", "", null);
            User Bayer6 = new User("11", "Andrey", "Andreev", "", Localisation.russian, "Pushkina str, 17-66, Gomel", "+375295656566", "", null);
            User Bayer7 = new User("12", "Egor", "Egorov", "", Localisation.russian, "Pushkina str, 33-14, Gomel", "+375294446566", "", null);
            User Bayer8 = new User("13", "Aleksey", "Alekseev", "", Localisation.russian, "Radosti str, 11-44, Gomel", "+375295656566", "", null);
            User Bayer9 = new User("14", "Ivan", "Ivanov", "", Localisation.russian, "Barykina str, 215-22, Gomel", "+375295699966", "", null);
            User Bayer10 = new User("15", "Alla", "Allova", "", Localisation.russian, "Sviridova str, 115-8, Gomel", "+37529658926", "", null);
            User Bayer11 = collectionTickets[0].Seller;

            collectionOrders = new List<Order>()
            {
                new Order(1, Status.rejected, Bayer1.UserId, Bayer1, "", collectionTickets[0]),
                new Order(2, Status.confirmed, Bayer2.UserId, Bayer2, "", collectionTickets[1]),
                new Order(3, Status.waiting_for_confirmation, Bayer3.UserId, Bayer3, "", collectionTickets[2]),
                new Order(4, Status.rejected, Bayer4.UserId, Bayer4, "", collectionTickets[3]),
                new Order(5, Status.rejected, Bayer5.UserId, Bayer5, "", collectionTickets[4]),
                new Order(6, Status.waiting_for_confirmation, Bayer6.UserId, Bayer6, "", collectionTickets[5]),
                new Order(7, Status.waiting_for_confirmation, Bayer7.UserId, Bayer7, "", collectionTickets[11]),
                new Order(8, Status.rejected, Bayer8.UserId, Bayer8, "", collectionTickets[7]),
                new Order(9, Status.confirmed, Bayer9.UserId, Bayer9, "", collectionTickets[8]),
                new Order(10, Status.waiting_for_confirmation, Bayer10.UserId, Bayer10, "", collectionTickets[9]),
                new Order(11, Status.confirmed, Bayer7.UserId, Bayer7, "", collectionTickets[19]),
                new Order(12, Status.confirmed, Bayer11.UserId, Bayer11, "", collectionTickets[7]),
                new Order(13, Status.waiting_for_confirmation, Bayer11.UserId, Bayer11, "", collectionTickets[8]),
                new Order(14, Status.confirmed, Bayer11.UserId, Bayer11, "", collectionTickets[21])
            };
        }

        public IEnumerable<Ticket> GetAllTicket => collectionTickets;

        public IEnumerable<Order> GetAllOrder => collectionOrders;

        public IEnumerable<User> GetAllUsers => collectionUsers;

        public static IEnumerable<User> AllUsers => collectionUsers;

        public IEnumerable<MyEvent> GetAllEvents => collectionEvents;

        public IEnumerable<MyEvent> GetEvents(string str)
        {
            List<MyEvent> me = new List<MyEvent>();
            foreach (Ticket xx in collectionTickets)
            {
                if (xx.MyEvent.GetEventTypeString().Equals(str))
                    me.Add(xx.MyEvent);
            }
            var myEv = me.GroupBy(xx => xx.Name).Select(zz => zz.First()).ToList();
            return myEv;
        }

        public MyEvent GetEvent(int id)
        {
            var tic = collectionEvents.Find(xx => xx.MyEventId == id);
            return tic;
        }
    }
}
