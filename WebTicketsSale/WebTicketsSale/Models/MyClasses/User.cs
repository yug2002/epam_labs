﻿using System.Security.Claims;

namespace WebTicketsSale.Models.MyClasses
{
    public enum Localisation { russian, belarussian, english }

    public class User/*: ClaimsIdentity*/
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NormLastName => LastName?.ToUpper();       
        public string Email { get; set; }
        public string NormEmail => Email?.ToUpper();
        public Localisation Localization { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string PasswordHash { get; set; }
        public UserRole UserRole { get; set; }

        public User()
        {
            UserId = "";
            Localization = Localisation.english;
            FirstName = default(string);
            LastName = default(string);            
            Email = default(string);
            Address = default(string);
            PhoneNumber = default(string);
            PasswordHash = default(string);
            UserRole = new UserRole();
            
        }

        public User(string id, string firstName, string lastName, string email, Localisation localization, string address, string phoneNumber, string passwordHash, UserRole userRole)
        {
            UserId = id;
            FirstName = firstName;
            LastName = lastName;            
            Email = email;
            Localization = localization;
            Address = address;
            PhoneNumber = phoneNumber;
            PasswordHash = passwordHash;
            UserRole = userRole;           
        }
    }
}
