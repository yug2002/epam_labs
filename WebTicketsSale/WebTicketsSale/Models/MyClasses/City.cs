﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTicketsSale.Models.MyClasses
{
    public class City
    {
        public int CityId { get; set; }
        public string Name { get; set; }
        public City()
        {
            CityId = 0;
            Name = default(string);
        }
        public City(int id, string name)
        {
            CityId = id;
            Name = name;
        }
    }
}
