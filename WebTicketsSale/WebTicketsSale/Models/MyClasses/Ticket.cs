﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTicketsSale.Models.MyClasses
{
    public class Ticket
    {
        public int Id { get; set; }

        public int MyEventId { get; set; }
        public MyEvent MyEvent { get; set; }

        public decimal Price { get; set; }

        public string UserId { get; set; }
        public User Seller { get; set; }

        public Ticket()
        {
            Id = 0;
            MyEventId = 0;
            MyEvent = null;
            Price = 0;
            UserId = "";
            Seller = null;
        }

        public Ticket(int id, int myEventId, MyEvent myEvent, decimal price, string userId, User seller)
        {
            Id = id;
            MyEventId = myEventId;
            MyEvent = myEvent;
            Price = price;
            UserId = userId;
            Seller = seller;
        }
    }
}
