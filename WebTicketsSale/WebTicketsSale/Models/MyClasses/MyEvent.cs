﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTicketsSale.Models.MyClasses
{
    public enum EventType { Concert,Exhibition,Show,Spectacle,Sport,Other}

    public class MyEvent:IComparable<MyEvent>
    {
        public int MyEventId { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }

        public int VenueId { get; set; }
        public Venue Venue { get; set; }

        public EventType EvType { get; set; }

        public MyEvent()
        {
            MyEventId = 0;
            Date = default(DateTime);
            Name = default(string);
            Banner = default(string);
            Description = default(string);
            VenueId = 0;
            Venue = null;
            EvType = EventType.Other;
        }

        public MyEvent(int id, string name, DateTime date, string banner, string description, int venId, Venue ven, EventType et)
        {
            MyEventId = id;
            Name = name;
            Date = date;
            Banner = banner;
            Description = description;
            VenueId = venId;
            Venue = ven;
            EvType = et;
        }

        public int CompareTo(MyEvent other)
        {
            return Name.CompareTo(other.Name);
        }
        public string GetEventTypeString()
        {
            return EvType.ToString();
        }
        public string GetDate
        {
            get { return (Date.Day + "." + Date.Month + "." + Date.Year); }
            private set { }
        }
    }
}
