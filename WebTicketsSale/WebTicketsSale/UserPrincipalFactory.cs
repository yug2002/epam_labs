﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebTicketsSale.Models.MyClasses;
using System.Security.Claims;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace WebTicketsSale
{
    public class UserPrincipalFactory : IUserClaimsPrincipalFactory<User>
    {
        private readonly IdentityOptions options;
        public UserPrincipalFactory(IOptions<IdentityOptions> optionsAccessor)
        {
            options = optionsAccessor?.Value ?? new IdentityOptions();
        }
        public Task<ClaimsPrincipal> CreateAsync(User user)
        {
            var identity = new ClaimsIdentity(
                options.Cookies.ApplicationCookieAuthenticationScheme,
                options.ClaimsIdentity.UserNameClaimType,
                options.ClaimsIdentity.RoleClaimType);
            identity.AddClaim(new Claim(options.ClaimsIdentity.UserIdClaimType, user.UserId));
            identity.AddClaim(new Claim(options.ClaimsIdentity.UserNameClaimType, user.LastName));
            identity.AddClaim(new Claim(options.ClaimsIdentity.RoleClaimType, user.UserRole.RoleName));

            var principal = new ClaimsPrincipal(identity);

            return Task.FromResult(principal);
        }
    }
}
